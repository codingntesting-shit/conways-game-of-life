#pragma once
#define Debug 1

class Board{
  int _size;
  int* _board;
  int* _board_old;
  char _get_tile_current(int x, int y);
  void _set_tile_current(int x, int y, char state);
  void _clear_board_current();
  int _is_neighbour(int x, int y);

public:
  Board(int size);
  char DEAD = '-';
  char ALIVE = '#';
  int get_size();
  int get_position(int x, int y);
  char get_tile(int x, int y);
  void set_tile(int x, int y, char state);
  void clear_board();
  void reset();
  void update_board();
  void set(int x, int y, int neighbours); 
  void new_board();
  void new_frame();
  void print_board();
};
