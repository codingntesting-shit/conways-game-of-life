#include "includes.h"


char Board::_get_tile_current(int x, int y) {return _board[get_position(x, y)];}
void Board::_set_tile_current(int x, int y, char state) {_board[get_position(x, y)] = state;}
void Board::_clear_board_current() {for (int x = 0; x < get_size(); x++) for (int y = 0; y < get_size(); y++) _set_tile_current(x, y, DEAD);}
int Board::_is_neighbour(int x, int y) {return (get_tile(x, y)==ALIVE )? 1 : 0;}


Board::Board(int size) : _size(size){
  _board = new int[size*size];
  _board_old = new int[size*size];
}

int Board::get_size() {return _size;}
int Board::get_position(int x, int y) {return x+y*get_size();}
char Board::get_tile(int x, int y) {return _board[get_position(x, y)];}
void Board::set_tile(int x, int y, char state) {_board_old[get_position(x, y)] = state;}
void Board::clear_board() {for (int x = 0; x < get_size(); x++) for (int y = 0; y < get_size(); y++) set_tile(x, y, DEAD);}
void Board::reset() {clear_board(); _clear_board_current();}
void Board::update_board() {for (int x = 0; x < get_size(); x++) for (int y = 0; y < get_size(); y++) _set_tile_current(x, y, get_tile(x, y));}
void Board::set(int x, int y, int neighbours) {
  switch (neighbours) {
  case 2:
    break;
  case 3:
    set_tile(x, y, ALIVE);
    break;
  default:
    set_tile(x, y, DEAD);
  }
}
void Board::new_board(){
  int neighbours;
  for (int y = 0; y < get_size(); y++)
    for (int x = 0; x < get_size(); x++){
      if (y == 0){
        if (x == 0){
          neighbours = _is_neighbour(x+1, y) + _is_neighbour(x+1, y+1) + _is_neighbour(x, y+1);
          set(x, y, neighbours);
          continue;
        }
        if (x == get_size()-1){
          neighbours = _is_neighbour(x-1, y) + _is_neighbour(x-1, y+1) + _is_neighbour(x, y+1);
          set(x, y, neighbours);
          continue;
        }
        neighbours = _is_neighbour(x-1, y) + _is_neighbour(x-1, y+1) + _is_neighbour(x, y+1) + _is_neighbour(x+1, y+1) + _is_neighbour(x+1, y);
        set(x, y, neighbours);
        continue;
      }
      if (y == get_size()-1){
        if (x == 0){
          neighbours = _is_neighbour(x+1, y) + _is_neighbour(x+1, y-1) + _is_neighbour(x, y-1);
          set(x, y, neighbours);
          continue;
        }
        if (x == get_size()-1){
          neighbours = _is_neighbour(x-1, y) + _is_neighbour(x-1, y-1) + _is_neighbour(x, y-1);
          set(x, y, neighbours);
          continue;
        }
        neighbours = _is_neighbour(x-1, y) + _is_neighbour(x-1, y-1) + _is_neighbour(x, y-1) + _is_neighbour(x+1, y-1) + _is_neighbour(x+1, y);
        set(x, y, neighbours);
        continue;
      }
      if (x == 0){
        neighbours =  _is_neighbour(x, y + 1) + _is_neighbour(x + 1, y + 1) + _is_neighbour(x + 1, y) +   _is_neighbour(x + 1, y - 1) + _is_neighbour(x, y - 1);
        set(x, y, neighbours);
        continue;
      }
      if (x == get_size()-1){
        neighbours =  _is_neighbour(x, y + 1) + _is_neighbour(x - 1, y + 1) + _is_neighbour(x - 1, y) +   _is_neighbour(x - 1, y - 1) + _is_neighbour(x, y - 1);
        set(x, y, neighbours);
        continue;
      }
      neighbours = _is_neighbour(x - 1, y - 1) +  _is_neighbour(x, y - 1) + _is_neighbour(x + 1, y - 1) + _is_neighbour(x + 1, y) + _is_neighbour(x + 1, y + 1) + _is_neighbour(x, y + 1) + _is_neighbour(x - 1, y + 1) + _is_neighbour(x - 1, y);
      set(x, y, neighbours);
    }
}
void Board::new_frame() {
  new_board();
  update_board();
}
void Board::print_board(){for (int y = 0; y < get_size(); y++){for (int x = 0; x < get_size(); x++) std::cout << get_tile(x, y); std::cout << std::endl;}}
