#include "includes.h"

int main(){
  Board board = 10;
  std::cout << board.ALIVE << std::endl;
  board.reset();
  board.set_tile(3, 3, board.ALIVE);
  board.set_tile(7, 7, board.ALIVE);
  board.set_tile(8, 7, board.ALIVE);
  board.set_tile(7, 8, board.ALIVE);
  board.set_tile(8, 8, board.ALIVE);
  board.update_board();
  std::cout << board.get_tile(8, 8) << std::endl;
  board.print_board();
}
